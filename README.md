# LIS-TGF algorithm

This code is based on the algorithm described in [C. K&ouml;hn et al., 2023. Employing Optical Lightning Data to identify lightning flashes associated to Terrestrial Gamma-ray Flashes.]. Based on a full ISS-LIS dataset, it will create a reduced list of lightning flashes, groups and events potentially associated to terrestrial gamma-ray flashes (TGFs).

## Getting started, pre-requisites

The main algorithm is encoded in ```tgf_algo_read_ncdf.py```. ```lis.py``` and ```file_management.py``` are used for I/O.

The following Python packages are needed:

```
requests
os
numpy
re
datetime
timedelta
netCDF4
fnmatch
sleep
convolve2d
```

ISS-LIS data can be downloaded from ```https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/```. ASIM data can be downloaded from the ASIM Science Data Center (ASDC), ```https://asdc.space.dtu.dk/```.


## Execution

The code needs Python 3 to run. Simply run 

```python3 tgf_algo_read_ncdf.py``` in the terminal or run the file in your favourite IDE.

The directory for input ISS-LIS files is specified through the variable ```storage folder``` in line 580 of ```tgf algo read ncdf.py```; the output folder ```result folder``` is specified in line 40 of the same file.

## Output

While the code runs, it generates four files per month ```mm``` for the given year ```yyyy```: 

 1. ```yyyy-mm_AllLISFlashes_ParameterSetting_V0.1.txt```: This file lists all LIS flashes per month and year, hence before applying the algorithm. In ascending order of columns, the file includes: the date and time of the flash, its latitude, longitutde, radiance, footprint size, child count (i.e. number of groups) as well as the time, latitutde, longitude, radiance, footprint size and child count (number of events) for the first group of that flash.

 2. ```yyyy-mm_AllLISGroups_ParameterSetting_V0.1.txt```: The structure of this file is the same as for ```yyyy-mm_AllLISFlashes_ParameterSetting_V0.1.txt```, but shows values for all groups of a flash, not only the first one.</li>

3. ```yyyy-mm_AssertionErrorsDuringAlgorithm_ParameterSetting_V0.1.txt```: This file summarizes the files which could not be handled by the presented
algorithm.

4. ```yyyy-mm_PotentialTGFs_ParameterSetting_V0.1.txt```: This file shows the same information as ```yyyy-mm_AllLISFlashes_ParameterSetting_V0.1.txt```, but only for the LIS flashes of the reduced dataset, i.e. after applying the algorithm and hence potentially being associated with the occurrence of TGFs. In addition, the last five columns mark with a “1” whether the spatial pattern is quadratic, rectangular, triangular, cornerless rectangular or other. In ```yyyy-mm_AllLISFlashes_ParameterSetting_V0.1.txt``` and ```yyyy-mm_AllLISGroups_ParameterSetting_V0.1.txt```, these values are set to “0” by default as it does not make sense to define the spatial pattern for all LIS flashes and groups. 

## Contributing

If you want to work on the algorithm, please use your own branches and commit into them. Changes will be merged into the ```main``` branch after thorough checking.

## Authors and acknowledgment

Main developers of the code: M. Heumesser, C. K&ouml;hn, O. Chanrion

Acknowledgments: This project has received funding from the European Union’s Horizon 2020 research and innovation program under the Marie Sklodowska-Curie grant agreement 722337.


