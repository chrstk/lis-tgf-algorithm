#!/usr/bin/python3
# -*- coding: utf-8 -*-

# classes and functions to download files from https://wiki.earthdata.nasa.gov/display/EL/How+To+Access+Data+With+Python  

import requests # get the requsts library from https://github.com/requests/requests
import os
import numpy as np

# overriding requests.Session.rebuild_auth to mantain headers when redirected
class SessionWithHeaderRedirection(requests.Session):
    AUTH_HOST = 'urs.earthdata.nasa.gov'
    def __init__(self, username, password):
        super().__init__()
        self.auth = (username, password)
 
   # Overrides from the library to keep headers when redirected to or from
   # the NASA auth host.
    def rebuild_auth(self, prepared_request, response):
        headers = prepared_request.headers
        url = prepared_request.url
 
        if 'Authorization' in headers:
            original_parsed = requests.utils.urlparse(response.request.url)
            redirect_parsed = requests.utils.urlparse(url)
 
            if (original_parsed.hostname != redirect_parsed.hostname) and \
                    redirect_parsed.hostname != self.AUTH_HOST and \
                    original_parsed.hostname != self.AUTH_HOST:
                del headers['Authorization']
  
        return
 
  
def get_files_for_day(day,index,storage_folder):

  read_valid_flag = 0		# flag when file has been found

  # create session with the user credentials that will be used to authenticate access to the data
  username = ""
  password= ""
  session = SessionWithHeaderRedirection(username, password)

  #just loop over all file numbers
  #set start and overall end index for each month per year (start index only for first day, otherwise taken from function argument; normally the whole loop is not performed because of break statement below)
  if (int(day.strftime("%Y"))==2019):
    if (int(day.strftime("%m"))==1):
      if (int(day.strftime("%d"))==1):
        start_index=11346
      else:
        start_index=index
      end_index = 11525
    if (int(day.strftime("%m"))==2):
      if (int(day.strftime("%d"))==1):
        start_index=11840
      else:
        start_index=index
      end_index = 12263
    if (int(day.strftime("%m"))==3):
      if (int(day.strftime("%d"))==1):
        start_index=12264
      else:
        start_index=index
      end_index = 12744
    if (int(day.strftime("%m"))==4):
      if (int(day.strftime("%d"))==1):
        start_index=12745
      else:
        start_index=index
      end_index = 13210
    if (int(day.strftime("%m"))==5):
      if (int(day.strftime("%d"))==1):
        start_index=13211
      else:
        start_index=index
      end_index = 13693
    if (int(day.strftime("%m"))==6):
      if (int(day.strftime("%d"))==1):
        start_index=13694
      else:
        start_index=index
      end_index = 14158
    if (int(day.strftime("%m"))==7):
      if (int(day.strftime("%d"))==1):
        start_index=14159
      else:
        start_index=index
      end_index = 14641
    if (int(day.strftime("%m"))==8):
      if (int(day.strftime("%d"))==1):
        start_index=14642
      else:
        start_index=index
      end_index = 15123
    if (int(day.strftime("%m"))==9):
      if (int(day.strftime("%d"))==1):
        start_index=15124
      else:
        start_index=index
      end_index = 15588
    if (int(day.strftime("%m"))==10):
      if (int(day.strftime("%d"))==1):
        start_index=15589
      else:
        start_index=index
      end_index = 16070
    if (int(day.strftime("%m"))==11):
      if (int(day.strftime("%d"))==1):
        start_index=16071
      else:
        start_index=index
      end_index = 16536
    if (int(day.strftime("%m"))==12):
      if (int(day.strftime("%d"))==1):
        start_index=16537
      else:
        start_index=index
      end_index = 17018



  #for number in range (start_index,end_index+1):

  #apparently, there was a renaming in LIS data -> not indices any more, but time (download loop needs to go through all timestamps)
  for number in range (0,235959+1):
    # the url of the file we wish to retrieve
    #url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V1.0_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_FIN_" + str(number)+ ".nc"
    #url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_??????_FIN.nc"
    
    #need check of no. of digits:
    
    if (number < 10):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_00000" + str(number) + "_FIN.nc"
    elif (number < 100):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_0000" + str(number) + "_FIN.nc"
    elif (number < 1000):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_000" + str(number) + "_FIN.nc"
    elif (number < 10900):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_00" + str(number) + "_FIN.nc"
    elif (number < 100000):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_0" + str(number) + "_FIN.nc"
    elif (number < 1000000):
      url = "https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/"+str(day.strftime("%Y"))+"/" + str (day.strftime("%m")) + str(day.strftime("%d")) + "/ISS_LIS_SC_V2.1_" +str(day.strftime("%Y"))+str (day.strftime("%m"))+ str(day.strftime("%d")) +"_" + str(number) + "_FIN.nc"
    
    
 
    #r  = requests.get("https://ghrc.nsstc.nasa.gov/pub/lis/iss/data/science/final/nc/2018/0101/")
    #data = r.text
    #print (data)
    #soup = BeautifulSoup(data)

    #for link in soup.find_all('a'):
      #print(link.get('href'))

    #sys.exit()
    # extract the filename from the url to be used when saving the file
    filename = url[url.rfind('/')+1:]  
 
  
    try:
        # submit the request using the session
        response = session.get(url, stream=True)
        print(response.status_code)
        #when file has been found, set read_valid_flag to 1
        #if (response.status_code==200): read_valid_flag = 1

        #when there was a previous valid file and now file not found (404), leave loop
        #if (response.status_code==404 and read_valid_flag == 1): 
          #break

 
  
        # raise an exception in case of http errors
        response.raise_for_status()  
 
  
        # save the file
        if (not os.path.isdir(str(storage_folder) + "/" + str(day.strftime("%Y")[2:4]) + "-" + str(day.strftime("%m")) + "-" + str(day.strftime("%d")))):
          os.mkdir(str(storage_folder) + "/" + str(day.strftime("%Y")[2:4]) + "-" + str(day.strftime("%m")) + "-" + str(day.strftime("%d")))
        with open(str(storage_folder) + "/" + str(day.strftime("%Y")[2:4]) + "-" + str(day.strftime("%m")) + "-" + str(day.strftime("%d")) + "/" + filename, 'wb') as fd:
            for chunk in response.iter_content(chunk_size=1024*1024):
                fd.write(chunk)

 
    except requests.exceptions.HTTPError as e:
        # handle any errors here
        print(e)

  #and return that index to download_files_for_month in tgf_algo_read_ncdf.py
  return number
 
"""
def read_tgflist():

  #read in tgf data from external file and save in array which is returned to analysis file
  
  input_tgf_list_file = open ('TGF_list_pos_appr_2019.txt','r')
  
  lines = []
  with open("TGF_list_pos_appr_2019.txt") as file:
      #lines = file.read().split()
      for line in file: 
          line = line.split() #split lines and store entry of lines into list
          lines.append(line) #append lines

  #return input_tgf_list_file.read()
  #return np.loadtxt('TGF_list_pos_appr_2019.txt',dtype='str')
  return lines

"""
  
data_folder = "."

