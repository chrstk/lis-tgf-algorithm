#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on      2019 Oct 21, 15:50

author: Matthias Heumesser
pkg versions: ../Data/Scripts/PyEnvConfig/2019-05-09_analysis.yml

Summary: File containing LIS extraction classes of the pyLAP package, contains
    LISNetCDFExtractor, LISTxtExtractor.
    All the classes open the respective file types and output the data, so it
        can be used in other scripts. Only minimal processing is done.
"""

import os
import re
import numpy as np
from datetime import datetime, timedelta
import netCDF4 as ncdf
import file_management as AB


class LISNetCDFExtractor():
    """
    Class to read ISS-LIS data from netCDF files and output a selection as
        txt files.
    """

    nc_folder = 'Sourcedata/LIS/1_netCDF_source'
    nc_parent_folder = os.path.join(AB.data_folder, nc_folder)
    lis_txt_fldr = 'Sourcedata/LIS/2_netCDF_selection'
    parent_folder = os.path.join(AB.data_folder, lis_txt_fldr)
    algo_parent_folder = os.path.join(
        AB.data_folder, 'Sourcedata/LIS/3_netCDF_LIS-TGF-Algorithm-Files')

    @staticmethod
    def sort_time(indata, leap=True, leapsec=10):
        """
        This function sorts the time for the other functions.
        returns list of times as datetime objects
        """

        inds_srtd = indata.argsort()
        sorted_data = indata[inds_srtd]
        if leap is True:
            return sorted_data - timedelta(seconds=leapsec), inds_srtd

        return sorted_data, inds_srtd

    @classmethod
    def utc_from_rawdata(cls, indata, level='event',
                         leap_sec_correction=True, leap_sec_amount=10):
        """
        Get the sorted UTC times from file as datetime objects and the indices
            of the correct order. This function uses the sort_time function.
        """
        time_lvl = {'event': 'lightning_event_TAI93_time',
                    'group': 'lightning_group_TAI93_time',
                    'flash': 'lightning_flash_TAI93_time',
                    'area': 'lightning_area_TAI93_time',
                    'one_sec': 'one_second_TAI93_time'}
        utc_times = ncdf.num2date(indata.variables[time_lvl[level]][:],
                                  indata.variables[time_lvl[level]].units,
                                  # the next two lines were added on 31Aug2020
                                  # --> necessary after update of package!
                                  only_use_cftime_datetimes=False,
                                  # added '.data' on 3.7.2021
                                  only_use_python_datetimes=True).data
        sorted_time, sorted_indices = cls.sort_time(utc_times,
                                                    leap=leap_sec_correction,
                                                    leapsec=leap_sec_amount)
        return sorted_time, sorted_indices

    @staticmethod
    def select_time(sorted_time_data, start, end):
        """
        Takes the output (tuple) of utc_from_rawdata and selects the intervall
            [start, end], start and end have to be datetime objects.
        """

        times = sorted_time_data[0]
        indices = sorted_time_data[1]
        selection_tf = (start <= times) & (times <= end)
        return times[selection_tf], indices[selection_tf]

    @staticmethod
    def built_var_list(indata, level='event',
                       variables=['location', 'radiance', 'address']):
        """
        Built the set of variables to work with and/or output. Default is
            ['location', 'radiance', 'address'].
        Currently this is restricted to the 'lightning' variables, i.e.
            'lightning_group_lon', 'lightning_group_radiance' and so forth.
            Other options are not available at the moment and would need a
            modification of the function.
        """
        full_names = []
        for var in variables:
            temp = 'lightning_' + level + '_' + var
            # variable selection without info what passes
            # and what gets discarded!!!
            if temp in indata.variables.keys():
                full_names.append(temp)
        return full_names

    @classmethod
    def level_output(cls, indata, level, variables, start_time, end_time):
        """
        Main function to output the selected ISS-LIS data to a txt file. It
            combines  all the data for the specified level, variables and time
            interval as preparation to write it to the file.
        """
        output_variables = cls.built_var_list(indata, level, variables)
        raw_time_data = cls.utc_from_rawdata(indata, level)
        output_time, output_indices = cls.select_time(raw_time_data,
                                                      start=start_time,
                                                      end=end_time)
        output = []
        for i, part in enumerate(output_time):
            line = []
            line.append(part.strftime("%Y-%m-%d %H:%M:%S.%f"))
            for var in output_variables:
                line.append(indata.variables[var][:][output_indices[i]])
            output.append(tuple(line))
        # output is a list of tuples
        coll_names = ['date_and_time'] + output_variables
        return output, coll_names

    @classmethod
    def ncdf_selection_to_txt(cls, ncfile, time_obj_start, time_obj_end,
                              level_list=['group'],
                              variables=['lat', 'lon', 'radiance', 'address']):
        """
        Function to open netCDF files, check data and output relevant parts.
        returns a txt file with data selection and saves it in the LIS folder

        possible variables are (time is always part of the output):
            'lat', 'lon', 'radiance', 'address' (pixel number),
            'parent_address', 'child_address', 'child_count', 'footprint',
            'approx_threshold', 'alert_flag', 'cluster_index', 'density_index',
            'noise_index', 'oblong_index', 'grouping_sequence',
            'grouping_status', 'glint_index';
        """
        day_str = re.findall(r'_[0-9]{8}_', ncfile)[0][1:-1]
        date_folder = day_str[2:4] + '-' + day_str[4:6] + '-' + day_str[6:]
        ncfile_path = os.path.join(cls.nc_parent_folder, date_folder, ncfile)


        time_str_start = time_obj_start.strftime('%Y-%m-%d_%H:%M:%S')
        time_str_end = time_obj_end.strftime('%Y-%m-%d_%H:%M:%S')

        messages = []
        with ncdf.Dataset(ncfile_path) as data:
            for lvl in level_list:
                try:
                    output_data = cls.level_output(data, lvl, variables,
                                                   time_obj_start,
                                                   time_obj_end)
                    if output_data[0] == []:
                        raise IOError('No data for level "' + lvl +
                                      '" and the selected interval: ' +
                                      time_str_start + ' to ' + time_str_end)
                except KeyError as kerr:
                    raise IOError('Specified variable "' + str(kerr.args[0]) +
                                  '" not in file.')

                f_name = time_str_start + '_to_' + time_str_end
                file_name = f_name + '_' + lvl + '-data.txt'
                out_file_path = os.path.join(folder_path, file_name)

                if not os.path.isfile(out_file_path):
                    with open(out_file_path, 'w') as output_file:
                        output_file.writelines(str.upper(lvl)+"\n")
                        output_file.writelines(output_header)
                        output_file.writelines(output_format_string % element
                                               for element in output_data[0])
                    endmsg1 = lvl + ': first and last time in output: '
                    endmsg2 = output_data[0][0][0] + ' - '
                    # print(endmsg1 + endmsg2 + output_data[0][-1][0])
                    messages.append(endmsg1 + endmsg2 + output_data[0][-1][0])
                else:
                    # print(lvl + '-file for the given time already exists.')
                    messages.append(lvl +
                                    '-file for the given time already exists.')
        return messages

    @classmethod
    def level_output_to_array(cls, file_data, grouping_level, vars_to_output):
        """
        Specific funtion for LIS-TGF-Serach algorithm. Based on 'level_output'
            function.

        Parameters
        ----------
        file_data : TYPE
            DESCRIPTION.
        grouping_level : TYPE
            DESCRIPTION.
        vars_to_output : TYPE
            DESCRIPTION.

        Returns
        -------
        lvl_out_arr : TYPE
            DESCRIPTION.
        times_sorted : TYPE
            DESCRIPTION.
        header : TYPE
            DESCRIPTION.

        """

        # preps: build correct var list, get times in chronologic order
        lis_var_names = cls.built_var_list(
            file_data, grouping_level, vars_to_output)
        times_sorted, sorted_indices = cls.utc_from_rawdata(
            file_data, grouping_level)

        # initialize array and fill with relative times
        # initialize header = dict with variable position in array
        lvl_out_arr = np.zeros((times_sorted.shape[0],
                                len(lis_var_names) + 1))  # +1 for time
        lvl_out_arr[:, 0] = np.array(
            [(time - times_sorted[0]).total_seconds()*1e3  # times in [ms]
             for time in times_sorted])
        header = {'time': 0}

        # fill with other variables in correct order (via sorted_indices)
        for index, lis_var in enumerate(lis_var_names, start=1):  # start 0->1
            lvl_out_arr[:, index] = \
                file_data.variables[lis_var][sorted_indices].data
            col_name = '_'.join(lis_var.split('_')[2:])  # make variable names
            header[col_name] = index  # .                  equivalent to input

        return lvl_out_arr, times_sorted, header

    @classmethod
    def ncdf_data_to_dicts(cls, ncdf_file, lis_levels, lis_variables):
        """
        Specific funtion for LIS-TGF-Serach algorithm. Similar to
            'ncdf_selection_to_txt' function but for use and not for saving.

        Parameters
        ----------
        ncdf_file_path : TYPE
            DESCRIPTION.
        lis_levels : TYPE
            DESCRIPTION.
        lis_variables : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """

        # initialize output dicts
        header_dict = {}
        array_output_dict = {}
        absolute_times_dict = {}

        day_str = re.findall(r'_[0-9]{8}_', ncdf_file)[0][1:-1]
        date_folder = day_str[2:4] + '-' + day_str[4:6] + '-' + day_str[6:]

        full_file_path = os.path.join(cls.algo_parent_folder,
                                      date_folder, ncdf_file)
        # open file and get dat for the different grouping levels
        with ncdf.Dataset(full_file_path) as data:
            for lvl in lis_levels:
                data_array, absolute_times, array_columns = \
                    cls.level_output_to_array(data, lvl, lis_variables)

                # save data to dicts
                array_output_dict[lvl] = data_array
                absolute_times_dict[lvl] = absolute_times
                header_dict[lvl] = array_columns

        # return filled dicts
        return array_output_dict, absolute_times_dict, header_dict


if __name__ == '__main__':
    LNE = LISNetCDFExtractor()
