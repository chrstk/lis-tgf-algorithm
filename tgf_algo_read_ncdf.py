#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 30 21:43:08 2021

@author: heumat
"""


import os
import fnmatch
from datetime import datetime, timedelta
from time import sleep
import numpy as np
from scipy.signal import convolve2d

from lis import LISNetCDFExtractor as LNE

import file_management as FM				




class SearchTGFinLISdata():

    # variables that are used in the algorithm
    algo_var_stems = ['datetime', 'lat', 'lon', 'radiance', 'footprint',
                      'address', 'parent_address', 'child_address',
                      'child_count', 'x_pixel', 'y_pixel']
    # variables that will be in the output list - same order as algo_var_stems
    out_var_stems = ['datetime', 'lat', 'lon', 'radiance', 'footprint',
                     'child_count']
    # output header
    forms = ['square', 'rectangle', 'triangle',
             'cornerless rectangle', 'other']
    data_header = ['_'.join([lvl, var]) for lvl, var in
                   zip(['flash']*len(out_var_stems) +
                       ['group']*len(out_var_stems), out_var_stems*2)] + forms

    result_folder = os.path.join('Results/TGF_Algorithm/MonthlyAlgorithmTriggers')

    @staticmethod
    def generate_month_boundaries(year):
        m_starts = []
        m_bounds = []
        #for January 2019, there are only 12 days, hence need to truncate afterwards
        if (year==2019):
          m_starts = m_starts + [datetime(year, 1, 1)] + [datetime(year, 1, 12+1)]
          m_starts = m_starts + [datetime(year, i, 1) for i in range(2, 13, 1)
                    ] + [datetime(year+1, 1, 1)]

          #in m_bounds, skyp 1:2 and 2:3 to avoid range from January,13th, until February, 1st
          m_bounds = m_bounds + [(start, end) for start, end in
                    zip(m_starts[0:1], m_starts[1:2])]

          m_bounds = m_bounds + [(start, end) for start, end in
                    zip(m_starts[2:-1], m_starts[3:])]
                    

        else:
          m_starts = m_starts + [datetime(year, i, 1) for i in range(1, 13, 1)
                    ] + [datetime(year+1, 1, 1)]

          m_bounds = [(start, end) for start, end in
                    zip(m_starts[:-1], m_starts[1:])]
        return m_bounds

    @classmethod
    def download_files_for_month(cls, days_of_month,storage_folder):

        errors = []
        #initial index for all months (can be improved later)
        index=11340
        for day in days_of_month:
            print(day.strftime("%Y-%m-%d"))
            try:
                #save final index for day in last_index_day
                last_index_day = FM.get_files_for_day(day,index,storage_folder)
                print('\tFiles downloaded for {}'.format(
                    day.strftime("%m-%d") ))
                sleep(5)
            except ConnectionError as ce:
                print('\t', ce)
                errors.append([day, ce.args[0]])
            #new index is the last index of the previous day
            index=last_index_day

        return errors

    @staticmethod
    def evaluate_all_LIS_flashes(flash_groups, grp_radiance_field=3,
                                   grp_address_field=5):

        # return address of selected group and its index in flash_groups;
        # selected group is the one with the highest radiance in the seq. (group not needed here; just for consistency with select_TGF_candidate_group)
        target_group_address = flash_groups[
            flash_groups[:, grp_radiance_field].argmax(),
            grp_address_field]

        target_group_index = np.nonzero(
            flash_groups[:, grp_address_field] == target_group_address
            )[0][0]
            
        #print (flash_groups)
        #sys.exit()
        return int(target_group_address), target_group_index


    @staticmethod
    def select_TGF_candidate_group(flash_groups, grp_radiance_field=3,
                                   grp_address_field=5):
        """
        Function to select the groups at the start of a flash and check if
            they fulfill the criteria to be potentially TGF related.

        Parameters
        ----------
        flash_groups : numpy array
            Array of groups in a flash with shape (n observations,
                                                   k group attributes).
        grp_radiance_field : int, optional
            Index of the radiance in the k group attributes. The default is 3.
        grp_address_field : int, optional
            Index of the address in the k group attributes. The default is 5.

        NB: Group times are always assumed to be at field index 0 and in [ms]!!

        Returns
        -------
        int or None
            Address of the selected group (target_group_address).
            Returns None if no group passed the selection.
        int or None
            Index of the group selected by the function (target_group_index).
            Returns None if no group passed the selection.
        """
        # Parameter definitions:
        # - Select groups at the start of the flash, i.e. in the first 16 ms
        # --> Effectively selects up to 9 (11) groups with 2.014 (1.495) ms
        #     between them (and mixtures), if they are all consecutive.
        fls_strt_int = 16.2  # in [ms]
        max_grp_cnt = 9
        # - Define max. integration time (and time between groups)
        # --> This should only be changed, if the algorithm is used for a
        #     different instrument, not to trim the performance!
        # --> LIS has time integrations of 1.495/1.511/1.999/2.014 -> take max
        max_integration_time = 2.014  # in [ms]
        # - Define max. temporal separation between pre activity and main peak
        # --> 5.6 corresponds to 2 groups between pre end and main pulse start
        pre_sep = 5.6  # in [ms]
        # - Define the max intensity of pre-activity compared to main
        pre_ratio = 0.22  # in [a.u.]

        # reference all group times to the start of the flash:
        group_times = flash_groups[:, 0] - flash_groups[0, 0]
        # use only the first 9 groups of the flash, because of the different
        # integration times, this needs a time (<16.2 ms) and count limit [:9]
        start_groups = flash_groups[group_times < fls_strt_int][:max_grp_cnt]
        # calculate times between the groups and subtract the max bin duration
        inter_grp_dt = np.diff(start_groups[:, 0]) - max_integration_time
        # set everything close to 0 (float issue) and below 0 to 0
        # -> 0 entries mean that the groups are consecutive integration cycles
        inter_grp_dt[inter_grp_dt < 0.0001] = 0
        # get sequence seperator indices of groups that are not consecutive
        # -> +1 becaues we want to find the group positions in the next step
        #    and not the position of the difference (np.diff reduced array
        #    length by 1)
        seq_sep_inds = np.nonzero(inter_grp_dt)[0] + 1
        # split groups into sequences of consecutive groups
        # -> empty ind_list arrays (when only one group or sequence of groups
        #    is available) just return the input
        con_grps_list = np.split(start_groups, seq_sep_inds)

        # based on the group separation, we can now select the right group to
        # investigate further:
        # depending on the length and intensity of the first sequence, it might
        # be pre-activity, then the next sequence is taken. Otherwise, we take
        # the earlist sequence
        # -> len(con_grps_list) == 0 can not occur as long as seq_sep_inds is
        #    calculated from start_groups
        if len(con_grps_list) == 1:  # only 1 sequence exists, we take it
            target_groups = con_grps_list[0]
        else:
            # we check that the pre-activity consists of 1-2 groups not earlier
            # than pre_sep before the main peak and not stronger than pre_ratio
            # compared to the main peak
            checks = [
                con_grps_list[0].shape[0] <= 2,
                # max_integration_time has to be subtracted from pre_sep
                # because it is also subtracted from all inter_grp_dt elements
                inter_grp_dt[seq_sep_inds[0]-1] < \
                pre_sep - max_integration_time,
                con_grps_list[0][:, grp_radiance_field].max() /
                con_grps_list[1][:, grp_radiance_field].max() < pre_ratio]
            # if all are ture, the first sequence is pre-activity, take next
            if np.asarray(checks).all():
                target_groups = con_grps_list[1]
            # if any is false, this is not pre-activity -> we take the sequence
            else:
                target_groups = con_grps_list[0]

        # sequences are not allowed to have more than 4 groups 
        if target_groups.shape[0] > 4:
            return None, None
        else:
            # return address of selected group and its index in flash_groups;
            # selected group is the one with the highest radiance in the seq.
            target_group_address = target_groups[
                target_groups[:, grp_radiance_field].argmax(),
                grp_address_field]
            target_group_index = np.nonzero(
                flash_groups[:, grp_address_field] == target_group_address
                )[0][0]
            return int(target_group_address), target_group_index

    @staticmethod
    def reconstruct_2d_repr_of_detections(array_of_event_coords):
        """
        Input has to be of the form (n, 2) at the moment.
        This function can be adjusted to include the radiance.

        returns the reconstructed array and its shape as array
        """
        # take the x and y pixel value of all events of the same group
        xy_pix = np.array(array_of_event_coords, dtype=int)
        print (xy_pix)
        print (xy_pix.max(0))
        print (xy_pix.min(0))
        print (xy_pix.max(0) - xy_pix.min(0) + 1)
        print (xy_pix - xy_pix.min(0))
        # reconstruct 2d array representing the group and set all values 1
        # --> works for all options (even single events) -> necessary for below
        reconstruction = np.zeros(xy_pix.max(0) - xy_pix.min(0) + 1)
        for i, j in xy_pix - xy_pix.min(0):
            reconstruction[i, j] = 1

        return reconstruction, np.asarray(reconstruction.shape)

    @staticmethod
    def calculate_pattern_limit_sums(reconstructed_array_shape):
        # The convolution over a full rectangle of 1s will have all 4s
        # (from integration over the 2x2 kernel). The convolved image has
        # a reduced shape (-1). This sum is the maximum one can have based
        # on a 2x2 kernal and 1s for triggered pixels!
        rectangle_sum = 4 * (reconstructed_array_shape - 1).prod()
        # The convolution sum over a triangular array follows a pattern of
        # 4s, 3s and 1s, which is the sum below. The shorter axis is taken
        # for non-quadratic arrayx.
        # This sum is the allowed minimum for the algorithm!
        smaller_dim = reconstructed_array_shape.min()
        triangle_sum = ((smaller_dim - 1) * 3 +
                        (smaller_dim - 1 - 1) * 1 +
                        np.arange(1, (smaller_dim - 2) + 1, 1).sum()*4)
        # The triangular form is the accepted minimum for rectangles. Since
        # LIS-groups have to be connected, this criterion also works well for
        # larger rectanlges >3x4 pixels. This minimum definition allows for
        # dark/not detected pixels due to the could or observation geometry.
        return rectangle_sum, triangle_sum

    @classmethod
    def convolution_to_select(cls, lis_event_coords_for_target_group):
        print (lis_event_coords_for_target_group)
        kernel = np.ones((2, 2))  # min. useful size, not normalized

        reconst, shape = cls.reconstruct_2d_repr_of_detections(
            lis_event_coords_for_target_group)
        print('Reconstruction:\n', reconst)  # input for convolution
        print('Shape: ', shape)

        # case selection by shape first to ensure fast evaluation
        if (shape < 2).any():  
            print('Skipping convolution - shape does not fit criteria! (M1)')
            return False, -1  # pattern passed the selection: False


        elif (shape > 6).any() or abs(np.diff(shape)) > 2:  
            print('Skipping convolution - shape does not fit criteria! (M2)')
            return False, -1  # pattern passed the selection: False
        

        else:
        #if (1==1):
            
            assert (((shape >= 2) & (shape <= 6)).all() &
                    (abs(np.diff(shape)) <= 2) & (shape.size == 2)), \
                'Error with shape of reconstructed array before convolution.'
            

            print('Convolution:')
            print(reconst)
            print(shape)
            conv_out = convolve2d(reconst, kernel, mode='valid')
            #conv_out = convolve2d(reconst, kernel, mode='full')
            print(conv_out)
            convo_sum = conv_out.sum()

            rectangle_sum, triang_sum = cls.calculate_pattern_limit_sums(shape)

            # extra for debugging or new functionality - currently not in use
            shape_options = {'square': False,
                             'rectangle': convo_sum == rectangle_sum,
                             'triangle': convo_sum == triang_sum,
                             'cornerless rectangle':
                                 convo_sum == rectangle_sum - 4,
                             'detected_pixel_count': reconst.sum()}
            if shape_options['rectangle'] and reconst.shape[0] == reconst.shape[1]:
                shape_options['square'] = True
                shape_options['rectangle'] = False

            print (triang_sum, convo_sum, rectangle_sum)
            # all patterns (sums) between triangle and rectangle are allowed
            if (triang_sum*(1-1.0) <= convo_sum) & (convo_sum <= rectangle_sum*(1+1.0)):
                print('Shape within borders, return True')
                print (shape_options)
                return True, shape_options  # pattern passed the selection: True  <<-----
            else:
                print('Shape does not fit criteria, return False')
                return False, -1  # pattern passed the selection: False

    @classmethod
    def coll_header_gen(cls,collumn_names, seperator='\t'):
      """
      This function produces a header string for my usual output txt files.
      It takes the list of collumns/header elements as input and returns
      one string that can directly be written to a file.
      """
      collumn_header = collumn_names[0]
      for element in collumn_names[1:]:
        collumn_header = collumn_header + seperator + element
      collumn_header = collumn_header + '\n'
      return collumn_header 


    @classmethod
    def data_repr_gen(cls,example_line, seperator='\t'):
      """
      This function produces the format string for the data output
      automatically. It takes the first ('0') entry of the level_output
      function as an input.
      e.g.:
      example = level_output(VARIABLES)[0]
      format_string = data_repr_gen(example)
      NB: The function does not work with the "location" variable since it is
      a 2D masked array! 'lat', 'lon' can be used!
      """
      data_type = {"<class 'str'>": "%s",
        "<class 'int'>": "%d",
        "<class 'float'>": "%f",
        "<class 'numpy.int8'>": "%d",
        "<class 'numpy.int32'>": "%d",
        "<class 'numpy.float32'>": "%f",
        "<class 'numpy.float64'>": "%f",
        "<class 'NoneType'>": "%s"
      }    

      data_format = data_type[repr(type(example_line[0]))]
      for entry in example_line[1:]:
        data_format = data_format + seperator + data_type[repr(type(entry))]

      data_format = data_format + '\n'

      return data_format 


    @classmethod
    def write_list_to_file(cls, file_name, data, header,
                           overwrite_existing=False):
        out_file_path = os.path.join(cls.result_folder, file_name)
        data_format = cls.data_repr_gen(data[0])
        header_fin = cls.coll_header_gen(header)
        if not os.path.isfile(out_file_path) or overwrite_existing:
            with open(out_file_path, 'w') as file_out:
                
                file_out.writelines(header_fin)
                file_out.writelines(data_format % tuple(line) for line in data)
                
                #file_out.writelines(str(line) for line in data)

    @classmethod
    def run_algo_on_files(cls, nc_file_names):

        data_to_save = []
        data_to_save_all_LIS_flashes = []	#make an array for ALL LIS flashes
        data_to_save_all_LIS_groups = []	#make an array for ALL LIS groups
        assertion_errors = []
        lis_file_errors = []

        # run algorithm on filelist
        for file in nc_file_names:
            print('\n----------\n\tNext File: {}\n----------\n'.format(file))

            # 2. import all the data form nc file - based on LNE
            try:
                detection_data, detection_datetimes, column_headers = LNE.ncdf_data_to_dicts(
                    file, ['event', 'group', 'flash'], cls.algo_var_stems)
                grp_header = column_headers['group']
                evt_header = column_headers['event']
            except KeyError as lis_in_ke:
                lis_file_errors = [file, lis_in_ke]
                continue

            # 3. algorithm
            # - loop over flashes
            for fl_ind, flash in enumerate(detection_data['flash'][:]):


                flash_adr = flash[column_headers['flash']['address']]
                grp_prnt_adr = grp_header['parent_address']
                grps_in_fls_tf = detection_data['group'][:, grp_prnt_adr] == flash_adr
                groups_in_flash = detection_data['group'][grps_in_fls_tf]


                #include save of all LIS flashes (for later postprocessing)
                group_adr, group_index = cls.evaluate_all_LIS_flashes(
                    groups_in_flash,
                    grp_radiance_field=grp_header['radiance'],
                    grp_address_field=grp_header['address'])

                tf_time = detection_data['group'][:, grp_header['address']] == group_adr

                fl_inds = np.array([column_headers['flash'][key] for key in cls.out_var_stems
                                    if key != 'datetime'])
                grp_inds = np.array([grp_header[key] for key in cls.out_var_stems
                                    if key != 'datetime'])
                                    
		#set shape(s) to zero (doesn't make sense for ALL LIS flashes)
                shape_1_hot=[0, 0, 0, 0, 0]

                output_flash = [detection_datetimes['flash'][fl_ind].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(flash[fl_inds])
                output_group = [detection_datetimes['group'][tf_time][0].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(*groups_in_flash[group_index, [grp_inds]])

                data_to_save_all_LIS_flashes.append(output_flash + output_group + shape_1_hot)  # add shape infos!

		#also save all LIS groups in output array (time won't be correct, since this is a constant for each flash; but not relevant for post-processing of all LIS groups)


                #for groups, sum over all groups in one flash (thus same group time for each flash); determine size through len(groups_in_flash)
                for i in range (0,len (groups_in_flash)):
                    output_flash = [detection_datetimes['flash'][fl_ind].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(flash[fl_inds])
                    output_group = [detection_datetimes['group'][tf_time][0].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(*groups_in_flash[i, [grp_inds]])



                    data_to_save_all_LIS_groups.append(output_flash + output_group + shape_1_hot)  # add shape infos!



                group_adr, group_index = cls.select_TGF_candidate_group(
                    groups_in_flash,
                    grp_radiance_field=grp_header['radiance'],
                    grp_address_field=grp_header['address'])

                # continue with next iteration if selection fails
                if group_adr is None or group_index is None:
                    continue

                evts_of_grp_TF = detection_data['event'][
                    :, evt_header['parent_address']] == group_adr

                print ([detection_datetimes['group'][tf_time][0].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(*groups_in_flash[i, [grp_inds]]))

                passed_selection, shapes = cls.convolution_to_select(
                    detection_data['event'][evts_of_grp_TF][
                        :, [evt_header['x_pixel'],
                            evt_header['y_pixel']
                            ]
                        ]
                    )
                    

                if passed_selection:
                    try:
                        assert shapes['detected_pixel_count'] == groups_in_flash[group_index, grp_header['child_count']]
                    except AssertionError as e:
                        print(e)
                        assertion_errors.append([file, detection_datetimes['flash'][fl_ind].strftime('%Y-%m-%d_%H:%M:%S.%f'),
                                                 shapes['detected_pixel_count'],
                                                 groups_in_flash[group_index, grp_header['child_count']]])

                    shape_1_hot = [int(shapes[form]) for form in cls.forms if form != 'other'] + [0]
                    if not sum(shape_1_hot):
                        shape_1_hot[-1] = 1
                    tf_time = detection_data['group'][:, grp_header['address']] == group_adr

                    fl_inds = np.array([column_headers['flash'][key] for key in cls.out_var_stems
                                        if key != 'datetime'])
                    grp_inds = np.array([grp_header[key] for key in cls.out_var_stems
                                        if key != 'datetime'])
                    output_flash = [detection_datetimes['flash'][fl_ind].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(flash[fl_inds])
                    output_group = [detection_datetimes['group'][tf_time][0].strftime('%Y-%m-%d_%H:%M:%S.%f')] + list(*groups_in_flash[group_index, [grp_inds]])
                    data_to_save.append(output_flash + output_group + shape_1_hot)  # add shape infos!

        return data_to_save, data_to_save_all_LIS_flashes, data_to_save_all_LIS_groups, assertion_errors, lis_file_errors

    @staticmethod
    def get_file_list_for_day(day_datetime):
        day_folder = os.path.join(LNE.algo_parent_folder,
                                  day_datetime.strftime("%y-%m-%d"))

        files = [fi for fi in os.listdir(day_folder) if
                 os.path.isfile(os.path.join(day_folder, fi)) and
                 fnmatch.fnmatch(fi, 'ISS_LIS_SC_V1.0_*_FIN_*.nc')]
        files.sort()

        return files

    @classmethod
    def main(cls, month_boundaries, storage_folder, skip_download=True,
             overwrite_output_files=False):

        download_errors = []
        LIS_file_errors = []


        for month_start, month_end in month_boundaries:
            print (str(month_start) + " " + str(month_end))

            days_in_month = [month_start + timedelta(days=x) for x in
                             range(0, (month_end - month_start).days, 1)]
                             

            # download of files (no re-download if they are present)
            if not skip_download:
                download_errors += cls.download_files_for_month(days_in_month,storage_folder)

            # initialize monthly variables and run algorithm per day of month
            monthly_triggers = []
            monthly_data_all_LIS_flashes = []
            monthly_data_all_LIS_groups = []
            monthly_errors = [['file', 'flash_datetime',
                               'detected_pixel_count', 'group_child_count']]
            
            for day in days_in_month:
                files = cls.get_file_list_for_day(day)
                day_triggers, day_data_all_LIS_flashes, day_data_all_LIS_groups, day_errors, day_file_errors = cls.run_algo_on_files(files)
                monthly_triggers += day_triggers
                monthly_data_all_LIS_flashes += day_data_all_LIS_flashes
                monthly_data_all_LIS_groups += day_data_all_LIS_groups
                monthly_errors += day_errors
                LIS_file_errors.append(day_file_errors)

            # output monthly result and error files
            out_file_name = '{}_PotentialTGFs_ParameterSetting_{}.txt'.format(
                month_start.strftime('%Y-%m'), 'V0.1')
            cls.write_list_to_file(out_file_name, monthly_triggers,
                                   STIL.data_header, overwrite_output_files)
            # write out all LIS flashes
            out_file_name_LIS = '{}_AllLISFlashes_ParameterSetting_{}.txt'.format(
                month_start.strftime('%Y-%m'), 'V0.1')
            cls.write_list_to_file(out_file_name_LIS, monthly_data_all_LIS_flashes,
                                   STIL.data_header, overwrite_output_files)

            # write out all LIS groups
            out_file_name_LIS = '{}_AllLISGroups_ParameterSetting_{}.txt'.format(
                month_start.strftime('%Y-%m'), 'V0.1')
            cls.write_list_to_file(out_file_name_LIS, monthly_data_all_LIS_groups,
                                   STIL.data_header, overwrite_output_files)


            ae_file_name = '{}_AssertionErrorsDuringAlgorithm_' \
                'ParameterSetting_{}.txt'.format(
                    month_start.strftime('%Y-%m'), 'V0.1')
            try:
                print ("here")
                cls.write_list_to_file(ae_file_name, monthly_errors[1:],
                                       monthly_errors[0], overwrite_output_files)
                print ("here2")
            except TypeError:
                print ("here3")
                print(monthly_errors)
            
        # return errors that occured during download, if any
        return download_errors, LIS_file_errors


if __name__ == '__main__':
    # initialization and preparation

    # define storage folder for downloading data
    storage_folder = 'Sourcedata/LIS/3_netCDF_LIS-TGF-Algorithm-Files'

    STIL = SearchTGFinLISdata()
    mbs2019 = STIL.generate_month_boundaries(2019)

    # run algorithm and output duration
    algo_start = datetime.now()  # START

    download_errors, file_errors = STIL.main(
        mbs2019, storage_folder, skip_download=True, overwrite_output_files=True)
    file_errors = [f for f in file_errors if f != []]

    algo_end = datetime.now()  # END
    mins, secs = divmod((algo_end - algo_start).total_seconds(), 60)
    hrs, mins = divmod(mins, 60)
    print('Total run time: {} h {} min {} sec'.format(hrs, mins, round(secs)))
    print('\nFile Errors:')
    for el in file_errors:
        print('\t{}: {}'.format(el[0], el[1]))
